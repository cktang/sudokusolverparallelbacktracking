#!/usr/bin/env python
import time
import numpy as np
from pycuda import driver, compiler, gpuarray, tools
import pycuda.autoinit
 
import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt

from tabulate import tabulate
def CustomPrintTime(py_time, naive_time):
## Print running time for cpu, naive and optimized algorithms
## Arguments: Each argument is a list of length 3 that contains the running times for three cases
 
    if len(py_time) != len(naive_time) or len(py_time) != 3:
        raise Exception('All lists should be 3, but get {}, {}'.format(len(py_time), len(naive_time)))
    headers = ['4x4', '9x9', '16x16']
    py_time = ['python'] + py_time
    naive_time = ['cuda'] + naive_time
    table = [py_time, naive_time]
    print "Time Taken by Kernels:"
    print tabulate(table, headers, tablefmt='fancy_grid').encode('utf-8')
 
##----------------------------------------------------------------------------------------------------------------------
 
def CustomPrintSpeedUp(naive_kernel, opt_kernel):
## Print the speed up
## Arguments: The first argument is the naive kernel running time and the second is optimized version
## Each argument is the length of 3.
 
    if len(opt_kernel) != len(naive_kernel) or len(opt_kernel) != 3:
        raise Exception('lenght of naive_kernel and opt_kernel must be 3, but get {}, {}'.format(len(naive_kernel), len(opt_kernel)))
    speedup = [[s * 1.0/t for s, t in zip(naive_kernel, opt_kernel)]]
    print "Speedup(Naive/Optimized):"
    header = ['4x4', '9x9', '16x16']
    print tabulate(speedup, header, tablefmt='fancy_grid').encode('utf-8')
 
##----------------------------------------------------------------------------------------------------------------------
class Node:
    position = (0,0)
    possibleValues = []

def buildGrid(gridString,size):
    grid = np.zeros((size, size))
    for i in range(size):
        for j in range(size):
            grid[i][j] = gridString[i*size+j]
    return grid

def buildGrid2(list2,size):
    grid = np.zeros((size, size))
    for i in range(size):
        for j in range(size):
            grid[i][j] = int(list2[i*size+j])
    return grid

def getEmptySpaces(grid):
    emptyspaceslist = []
    for i in range(size):
        for j in range(size):
            if(grid[i][j]==0):
                emptyspaceslist.append((i,j))
    return emptyspaceslist

#create nodes for empty spaces
def createEmptySpacesNode(grid,size,flag):
    empty_spaces = []
    for i in range(size):
        for j in range(size):
            if(grid[i][j] == 0):
                values = findPossibleValues(grid,(i,j))
                if(len(values)==1 and flag==False):
                    grid[i][j] = values[0]
                else:
                    n = Node()
                    n.position = (i,j)
                    n.possibleValues = values
                    empty_spaces.append(n)
    return empty_spaces

def createEmptySpacesPositionList(grid,size,flag):
    empty_spaces_position_i = []
    empty_spaces_position_j = []
    for i in range(size):
        for j in range(size):
            if(grid[i][j] == 0):
                values = findPossibleValues(grid,(i,j))
                if(len(values)==1 and flag==False):
                    grid[i][j] = values[0]
                else:
                    empty_spaces_position_i.append(i)
                    empty_spaces_position_j.append(j)
    return (empty_spaces_position_i,empty_spaces_position_j)

def findPossibleValues(grid,position):
    values = [x for x in range(1,size+1)]
    row = position[0]
    col = position[1]
    
    #check for row
    for i in range(size):
        if(grid[row][i] in values):
            values.remove(grid[row][i])
    #check for column
    for i in range(size):
        if(grid[i][col] in values):
            values.remove(grid[i][col])
    #this square
    sqrtSize=np.sqrt(size).astype(int)
    startRow=(row//sqrtSize)*sqrtSize
    startCol=(col//sqrtSize)*sqrtSize
    for i in range(sqrtSize):
        for j in range(sqrtSize):
            if(grid[i+startRow][j+startCol] in values):
                values.remove(grid[i+startRow][j+startCol])
    return values

#sort empty_spaces list by number of possible values
def getPossibleValues(n):
    possibleValueList = n.possibleValues
    return len(possibleValueList)

#run sudoku
def runSudoku(grid,empty_spaces):
    stack = []
    while(len(empty_spaces)!=0):
        #move empty_space node to stack
        stack.append(empty_spaces[0])
        empty_spaces.pop(0)
        
        #get bottom of stack 
        currentStackIndex=len(stack)-1

        stackNode = stack[currentStackIndex]
        
        #check possibleValues
        pValues = stackNode.possibleValues
        while (len(pValues) == 0):
            #backtrack
            n = stack.pop()
            nPos = n.position
            grid[nPos[0]][nPos[1]] = 0
            
            empty_spaces.insert(0,n)
            
            currentStackIndex=len(stack)-1
            stackNode = stack[currentStackIndex]
            pValues = stackNode.possibleValues
            #change value of bottom of current nodes 
  
        #else:
        pos = stackNode.position
        grid[pos[0]][pos[1]] = stackNode.possibleValues[0]

        pValues.pop(0)
        stackNode.possibleValues = pValues # just in case 

        #recalcualte possible values for top of empty_spaces
        if(len(empty_spaces)>0):
            for n in empty_spaces:
                n.possibleValues = findPossibleValues(grid,n.position)
            empty_spaces.sort(key=getPossibleValues)
            
    return grid
def checkGrid(grid,size):
    #check rows
    for i in range(size):
        listR = [x for x in range(1,size+1)]
        listC = [x for x in range(1,size+1)]
        for j in range(size):
            if grid[i][j] in listR:
                listR.remove(grid[i][j])
            else:
                return False
            if grid[j][i] in listC:
                listC.remove(grid[j][i])
            else:
                return False
    #check squares:
    sqrtSize=np.sqrt(size).astype(int)
    rowList=[x*sqrtSize for x in range(0,sqrtSize)]
    colList=[x*sqrtSize for x in range(0,sqrtSize)]
    for row in rowList:
        for col in colList:
            values = [x for x in range(1,size+1)]
            startRow=(row//sqrtSize)*sqrtSize
            startCol=(col//sqrtSize)*sqrtSize
            for i in range(sqrtSize):
                for j in range(sqrtSize):
                    if(grid[i+startRow][j+startCol] in values):
                        values.remove(grid[i+startRow][j+startCol])
                    else:
                        return False
    return True

def decodeMsg(grid,size):
    gridString=""
    for i in range(size):
        if(grid[i][i]==1):
            gridString+='N'
        elif(grid[i][i]==2):
            gridString+='H'
        elif(grid[i][i]==3):
            gridString+='R'
        elif(grid[i][i]==4):
            gridString+='I'
        elif(grid[i][i]==5):
            gridString+='D'
        elif(grid[i][i]==6):
            gridString+='U'
        elif(grid[i][i]==7):
            gridString+='S'
        elif(grid[i][i]==8):
            gridString+='G'
        elif(grid[i][i]==9):
            gridString+='O'
    # N-1,H-2,R-3,I-4,D-5, U-6,S-7,G-8,O-9
    print(gridString)
    return gridString

def encodeMsg(gridString):
    gridString=list(gridString)
    count=0
    for char in gridString:
        if(char=='N'):
            gridString[count]='1'
        elif(char=='H'):
            gridString[count]='2'
        elif(char=='R'):
            gridString[count]='3'
        elif(char=='I'):
            gridString[count]='4'
        elif(char=='D'):
            gridString[count]='5'
        elif(char=='U'):
            gridString[count]='6'
        elif(char=='S'):
            gridString[count]='7'
        elif(char=='G'):
            gridString[count]='8'
        elif(char=='O'):
            gridString[count]='9'
        count=count+1
    temp="".join(gridString)
    return temp
# Define the CUDA kernel
kernel_code_template = """
__constant__ int empty_spaces_i[%(empty_spaces_length)s];
__constant__ int empty_spaces_j[%(empty_spaces_length)s];

//Compute the Possible Values List
//True-constraints exist
//False-constraints do not exist, possible to be the value
//Set all values that appeared in the particular row,column and box to true
__device__ void computePossibleValues(int* grid, bool* possibleValueList, int i, int j)
{
    unsigned int a,b;
    unsigned int sqrtSize=(unsigned int)sqrt((float)%(size)s);

    for(a=0;a<%(size)s;a++)
    {
        possibleValueList[a]=false;
    }
    for(a=0;a<%(size)s;a++)
    {
        if(grid[i*%(size)s+a]>0)
            possibleValueList[grid[i*%(size)s+a]-1]=true;
        if(grid[a*%(size)s+j]>0)
        possibleValueList[grid[a*%(size)s+j]-1]=true;
    }
    unsigned int startRow=(i/sqrtSize)*sqrtSize;
    unsigned int startCol=(j/sqrtSize)*sqrtSize;
    for(a=0;a<sqrtSize;a++)
    {
        for(b=0;b<sqrtSize;b++)
        {
            if(grid[(a+startRow)*%(size)s+b+startCol]>0)
                possibleValueList[grid[(a+startRow)*%(size)s+b+startCol]-1]=true;
        }
    }
}
//if found possibleValue that dont have constraints:
//set path=true to indicate there is valid path to move on
//otherwise, break loop
__device__ bool findPossibleValues(int &idx, bool* possibleValueList)
{
    bool path=false;
    for(;idx<%(size)s;idx++)
    {
        if(possibleValueList[idx]==false)
        {
            path=true;
            break;
        }
    }
    return path;
}

__global__ void generateBoard(int* grid,int* out,int* threadCount) {
    unsigned int empty_spaces_count=0;
    bool path=false;
    unsigned int x,y;

    int idx;
    int i,j;

    bool possibleValueList[%(size)s];

    int possibleValuesIdx[%(size)s*%(size)s];

    //initialise possibleValueIdx array to 0
    for(unsigned int a=0;a<%(size)s*%(size)s;a++)
    {
        possibleValuesIdx[a]=0;
    }
    while((threadCount[0]<%(maxboard)s))
    {
        //fill in emptyspaces and call more
        i = empty_spaces_i[empty_spaces_count];
        j = empty_spaces_j[empty_spaces_count];

        computePossibleValues(grid,possibleValueList,i,j);

        //start from possibleValuesIdx to prevent doing the same node again for backtracking
        idx=possibleValuesIdx[i*%(size)s+j];
        path=findPossibleValues(idx,possibleValueList);

        if(empty_spaces_count==%(maxdepth)s) //store results and start backtrack
        {
            path=false;

            //output grid for every new board
            for (x=0;x<%(size)s;x++)
            {
                for(y=0;y<%(size)s;y++)
                {
                    out[threadCount[0]*%(size)s*%(size)s+y*%(size)s+x]=grid[y*%(size)s+x];
                }
            }
            threadCount[0]+=1;
        }
        while(path==false)
        {
            //backtrack
            possibleValuesIdx[i*%(size)s+j]=0; //reset start index
            grid[i*%(size)s+j] = 0;  //reset grid
            empty_spaces_count -=1;

            if(empty_spaces_count==-1)
                break;

            i = empty_spaces_i[empty_spaces_count];
            j = empty_spaces_j[empty_spaces_count];

            computePossibleValues(grid,possibleValueList,i,j);

            idx=possibleValuesIdx[i*%(size)s+j];
            path=findPossibleValues(idx,possibleValueList);
        }    
        if(empty_spaces_count==-1)
            break;
        grid[i*%(size)s+j] = idx+1; //set value
        possibleValuesIdx[i*%(size)s+j]=idx+1; //next time check for possible values, start from next value
        empty_spaces_count +=1;
    }
}

__global__ void runSudokuKernel(int* more_grid,int* flag,int* actualgrid) {
    //keep private copy of specific board/grid
    const int x = blockIdx.x*blockDim.x+threadIdx.x;
    const int totalSize = %(size)s*%(size)s;
    int grid[totalSize];
    bool checkValidInput=false;
    for(int i =0;i<totalSize;i++){
        grid[i] = more_grid[x*totalSize+i];
        if(grid[i]!=0)
            checkValidInput=true;
    }

    if(checkValidInput)
    {
        int empty_spaces_count=%(maxdepth)s;
        int idx;
        int i,j;

        int possibleValuesIdx[%(size)s*%(size)s];

        //initialise possibleValueIdx array to 0
        for(unsigned int a=0;a<%(size)s*%(size)s;a++)
        {
            possibleValuesIdx[a]=0;
        }

        bool possibleValueList[%(size)s];
        bool path;
        while(empty_spaces_count<%(empty_spaces_length)s&&flag[0]==0)
        {    
            i = empty_spaces_i[empty_spaces_count];
            j = empty_spaces_j[empty_spaces_count];

            computePossibleValues(grid,possibleValueList,i,j);

            //start from possibleValuesIdx to prevent doing the same node again for backtracking
            idx=possibleValuesIdx[i*%(size)s+j];
            path=findPossibleValues(idx,possibleValueList);

            while(path==false)
            {
                //backtrack
                possibleValuesIdx[i*%(size)s+j]=0; //reset start index
                grid[i*%(size)s+j] = 0;  //reset grid
                empty_spaces_count -=1;
                if(empty_spaces_count<%(maxdepth)s)
                    break;
                i = empty_spaces_i[empty_spaces_count];
                j = empty_spaces_j[empty_spaces_count];

                computePossibleValues(grid,possibleValueList,i,j);

                idx=possibleValuesIdx[i*%(size)s+j];
                path=findPossibleValues(idx,possibleValueList);

            }    
            if(empty_spaces_count<%(maxdepth)s)
                break;
            grid[i*%(size)s+j] = idx+1; //set value
            possibleValuesIdx[i*%(size)s+j]=idx+1; //next time check for possible values, start from next value
            empty_spaces_count +=1;
        }
        if(empty_spaces_count>%(maxdepth)s)
        {
            flag[0]=1;
            for(i=0;i<totalSize;i++)
                actualgrid[i]=grid[i];
        }
    }
}

__global__ void decodeMsg(int* grid, char* inputString){
    int id=threadIdx.x;
    int value=grid[id*%(size)s+id];

    if(value==1)
        inputString[id]='N';
    else if(value==2)
        inputString[id]='H';
    else if(value==3)
        inputString[id]='R';
    else if(value==4)
        inputString[id]='I';
    else if(value==5)
        inputString[id]='D';
    else if(value==6)
        inputString[id]='U';
    else if(value==7)
        inputString[id]='S';
    else if(value==8)
        inputString[id]='G';
    else if(value==9)
        inputString[id]='O';
}
"""

kernel_code_naive_template = """

//Compute the Possible Values List
//True-constraints exist
//False-constraints do not exist, possible to be the value
//Set all values that appeared in the particular row,column and box to true
__device__ void computePossibleValues(int* grid, bool* possibleValueList, int i, int j)
{
    unsigned int a,b;
    unsigned int sqrtSize=(unsigned int)sqrt((float)%(size)s);

    for(a=0;a<%(size)s;a++)
    {
        possibleValueList[a]=false;
    }
    for(a=0;a<%(size)s;a++)
    {
        if(grid[i*%(size)s+a]>0)
            possibleValueList[grid[i*%(size)s+a]-1]=true;
        if(grid[a*%(size)s+j]>0)
        possibleValueList[grid[a*%(size)s+j]-1]=true;
    }
    unsigned int startRow=(i/sqrtSize)*sqrtSize;
    unsigned int startCol=(j/sqrtSize)*sqrtSize;
    for(a=0;a<sqrtSize;a++)
    {
        for(b=0;b<sqrtSize;b++)
        {
            if(grid[(a+startRow)*%(size)s+b+startCol]>0)
                possibleValueList[grid[(a+startRow)*%(size)s+b+startCol]-1]=true;
        }
    }
}
//if found possibleValue that dont have constraints:
//set path=true to indicate there is valid path to move on
//otherwise, break loop
__device__ bool findPossibleValues(int &idx, bool* possibleValueList)
{
    bool path=false;
    for(;idx<%(size)s;idx++)
    {
        if(possibleValueList[idx]==false)
        {
            path=true;
            break;
        }
    }
    return path;
}

__global__ void generateBoard(int* grid,int* out,int* threadCount,int* empty_spaces_i, int* empty_spaces_j) {
    unsigned int empty_spaces_count=0;
    bool path=false;
    unsigned int x,y;

    int idx;
    int i,j;

    bool possibleValueList[%(size)s];

    int possibleValuesIdx[%(size)s*%(size)s];

    //initialise possibleValueIdx array to 0
    for(unsigned int a=0;a<%(size)s*%(size)s;a++)
    {
        possibleValuesIdx[a]=0;
    }
    while((threadCount[0]<%(maxboard)s))
    {
        //fill in emptyspaces and call more
        i = empty_spaces_i[empty_spaces_count];
        j = empty_spaces_j[empty_spaces_count];

        computePossibleValues(grid,possibleValueList,i,j);

        //start from possibleValuesIdx to prevent doing the same node again for backtracking
        idx=possibleValuesIdx[i*%(size)s+j];
        path=findPossibleValues(idx,possibleValueList);

        if(empty_spaces_count==%(maxdepth)s) //store results and start backtrack
        {
            path=false;

            //output grid for every new board
            for (x=0;x<%(size)s;x++)
            {
                for(y=0;y<%(size)s;y++)
                {
                    out[threadCount[0]*%(size)s*%(size)s+y*%(size)s+x]=grid[y*%(size)s+x];
                }
            }
            threadCount[0]+=1;
        }
        while(path==false)
        {
            //backtrack
            possibleValuesIdx[i*%(size)s+j]=0; //reset start index
            grid[i*%(size)s+j] = 0;  //reset grid
            empty_spaces_count -=1;

            if(empty_spaces_count==-1)
                break;

            i = empty_spaces_i[empty_spaces_count];
            j = empty_spaces_j[empty_spaces_count];

            computePossibleValues(grid,possibleValueList,i,j);

            idx=possibleValuesIdx[i*%(size)s+j];
            path=findPossibleValues(idx,possibleValueList);
        }    
        if(empty_spaces_count==-1)
            break;
        grid[i*%(size)s+j] = idx+1; //set value
        possibleValuesIdx[i*%(size)s+j]=idx+1; //next time check for possible values, start from next value
        empty_spaces_count +=1;
    }
}

__global__ void runSudokuKernel(int* more_grid,int* actualgrid, int* empty_spaces_i, int* empty_spaces_j) {
    //keep private copy of specific board/grid
    const int x = blockIdx.x*blockDim.x+threadIdx.x;
    const int totalSize = %(size)s*%(size)s;
    int grid[totalSize];
    bool checkValidInput=false;
    for(int i =0;i<totalSize;i++){
        grid[i] = more_grid[x*totalSize+i];
        if(grid[i]!=0)
            checkValidInput=true;
    }

    if(checkValidInput)
    {
        int empty_spaces_count=%(maxdepth)s;
        int idx;
        int i,j;

        int possibleValuesIdx[%(size)s*%(size)s];

        //initialise possibleValueIdx array to 0
        for(unsigned int a=0;a<%(size)s*%(size)s;a++)
        {
            possibleValuesIdx[a]=0;
        }

        bool possibleValueList[%(size)s];
        bool path;
        while(empty_spaces_count<%(empty_spaces_length)s)
        {    
            i = empty_spaces_i[empty_spaces_count];
            j = empty_spaces_j[empty_spaces_count];

            computePossibleValues(grid,possibleValueList,i,j);

            //start from possibleValuesIdx to prevent doing the same node again for backtracking
            idx=possibleValuesIdx[i*%(size)s+j];
            path=findPossibleValues(idx,possibleValueList);

            while(path==false)
            {
                //backtrack
                possibleValuesIdx[i*%(size)s+j]=0; //reset start index
                grid[i*%(size)s+j] = 0;  //reset grid
                empty_spaces_count -=1;
                if(empty_spaces_count<%(maxdepth)s)
                    break;
                i = empty_spaces_i[empty_spaces_count];
                j = empty_spaces_j[empty_spaces_count];

                computePossibleValues(grid,possibleValueList,i,j);

                idx=possibleValuesIdx[i*%(size)s+j];
                path=findPossibleValues(idx,possibleValueList);

            }    
            if(empty_spaces_count<%(maxdepth)s)
                break;
            grid[i*%(size)s+j] = idx+1; //set value
            possibleValuesIdx[i*%(size)s+j]=idx+1; //next time check for possible values, start from next value
            empty_spaces_count +=1;
        }
        if(empty_spaces_count>%(maxdepth)s)
        {
            for(i=0;i<totalSize;i++)
                actualgrid[i]=grid[i];
        }
    }
}

__global__ void decodeMsg(int* grid, char* inputString){
    int id=threadIdx.x;
    int value=grid[id*%(size)s+id];

    if(value==1)
        inputString[id]='N';
    else if(value==2)
        inputString[id]='H';
    else if(value==3)
        inputString[id]='R';
    else if(value==4)
        inputString[id]='I';
    else if(value==5)
        inputString[id]='D';
    else if(value==6)
        inputString[id]='U';
    else if(value==7)
        inputString[id]='S';
    else if(value==8)
        inputString[id]='G';
    else if(value==9)
        inputString[id]='O';
}
"""
####################################### Sudoku Input Grid ##########################################
gridString = '4000004000030200'
size1 = int(np.sqrt(len(gridString)))
grid1 = buildGrid(gridString,size1)
# gridString = '003020600900305001001806400008102900700000008006708200002609500800203009005010300'
gridString = '000001000020000008691200000000000014102506003800020506005000000730000000006319405'
size2 = int(np.sqrt(len(gridString)))
grid2 = buildGrid(gridString,size2)

gridString = '''0 15 0 1 0 2 10 14 12 0 0 0 0 0 0 0
0 6 3 16 12 0 8 4 14 15 1 0 2 0 0 0
14 0 9 7 11 3 15 0 0 0 0 0 0 0 0 0
4 13 2 12 0 0 0 0 6 0 0 0 0 15 0 0
0 0 0 0 14 1 11 7 3 5 10 0 0 8 0 12
3 16 0 0 2 4 0 0 0 14 7 13 0 0 5 15
11 0 5 0 0 0 0 0 0 9 4 0 0 6 0 0
0 0 0 0 13 0 16 5 15 0 0 12 0 0 0 0
0 0 0 0 9 0 1 12 0 8 3 10 11 0 15 0
2 12 0 11 0 0 14 3 5 4 0 0 0 0 9 0
6 3 0 4 0 0 13 0 0 11 9 1 0 12 16 2
0 0 10 9 0 0 0 0 0 0 12 0 8 0 6 7
12 8 0 0 16 0 0 10 0 13 0 0 0 5 0 0
5 0 0 0 3 0 4 6 0 1 15 0 0 0 0 0
0 9 1 6 0 14 0 11 0 0 2 0 0 0 10 8
0 14 0 0 0 13 9 0 4 12 11 8 0 0 2 0'''
temp = gridString.replace('\n'," ")
nums= temp.split(" ")
size3=int(np.sqrt(len(nums)))
grid3 = buildGrid2(nums,size3)

gridArray=[grid1,grid2,grid3]
sizeArray=[size1,size2,size3]
####################################### Main Code: Variable Grid Size ###########################################
pythontimes=[]
cudatimes=[]
cudanaivetimes=[]
X_AXIS_RECORD=[]
ITERATION=3

MAXBOARD = 3000
MAXDEPTH = 4
for i in range(ITERATION):
    grid=gridArray[i]
    size=sizeArray[i]
    X_AXIS_RECORD.append(((2+i)**2)**2)
    # print(grid,size)

    empty_spaces=createEmptySpacesNode(grid,size,False)
    empty_spaces.sort(key=getPossibleValues)

    # Define the input and output to process
    grid=gridArray[i]
    h_empty_spaces_i,h_empty_spaces_j= createEmptySpacesPositionList(grid,size,True)
    print(len(empty_spaces),len(h_empty_spaces_i))

    #optimization declaration
    h_boardcount=np.zeros((1,1)).astype(np.int32)
    h_flag=np.zeros((1,1)).astype(np.int32)
    h_grid = np.array(grid).flatten().astype(np.int32)
    h_possibleValuesIdx=np.zeros((size,size)).astype(np.int32)
    h_moregrid = np.zeros((MAXBOARD,size,size)).astype(np.int32)
    h_actualgrid=np.zeros_like(h_grid).astype(np.int32)

    d_boardcount = gpuarray.to_gpu(h_boardcount)
    d_flag = gpuarray.to_gpu(h_flag)
    d_grid = gpuarray.to_gpu(h_grid)
    d_possibleValuesIdx=gpuarray.to_gpu(h_possibleValuesIdx)
    d_moregrid = gpuarray.to_gpu(h_moregrid)
    d_actualgrid=gpuarray.to_gpu(h_actualgrid)

    #naive declaration
    h_boardcount_naive=np.zeros((1,1)).astype(np.int32)
    h_flag_naive=np.zeros((1,1)).astype(np.int32)
    h_grid_naive = np.array(grid).flatten().astype(np.int32)
    h_possibleValuesIdx_naive=np.zeros((size,size)).astype(np.int32)
    h_moregrid_naive = np.zeros((MAXBOARD,size,size)).astype(np.int32)
    h_actualgrid_naive=np.zeros_like(h_grid).astype(np.int32)

    d_boardcount_naive = gpuarray.to_gpu(h_boardcount_naive)
    d_flag_naive = gpuarray.to_gpu(h_flag_naive)
    d_grid_naive = gpuarray.to_gpu(h_grid_naive)
    d_possibleValuesIdx_naive=gpuarray.to_gpu(h_possibleValuesIdx_naive)
    d_moregrid_naive = gpuarray.to_gpu(h_moregrid_naive)
    d_actualgrid_naive=gpuarray.to_gpu(h_actualgrid_naive)
    d_empty_spaces_i_naive = gpuarray.to_gpu(np.array(h_empty_spaces_i).astype(np.int32))
    d_empty_spaces_j_naive = gpuarray.to_gpu(np.array(h_empty_spaces_j).astype(np.int32))

    # get the kernel code from the template 
    # by specifying the constant MATRIX_SIZE
    empty_spaces_length=len(h_empty_spaces_i);
    kernel_code = kernel_code_template % {
        'size': size,
        'maxboard':MAXBOARD,
        'maxdepth':MAXDEPTH,
        'empty_spaces_length':empty_spaces_length
        }
        
    # compile the kernel code
    mod = compiler.SourceModule(kernel_code)
    d_empty_spaces_i = mod.get_global('empty_spaces_i')[0]
    d_empty_spaces_j = mod.get_global('empty_spaces_j')[0]
    driver.memcpy_htod(d_empty_spaces_i,  np.array(h_empty_spaces_i).astype(np.int32))
    driver.memcpy_htod(d_empty_spaces_j,  np.array(h_empty_spaces_j).astype(np.int32))

    # # get the kernel function from the compiled module
    runSudokuKernelFn = mod.get_function("runSudokuKernel")
    generateBoardFn = mod.get_function("generateBoard")

    #compile naive code

    kernel_code_naive = kernel_code_naive_template % {
        'size': size,
        'maxboard':MAXBOARD,
        'maxdepth':MAXDEPTH,
        'empty_spaces_length':empty_spaces_length
        }
    mod_naive = compiler.SourceModule(kernel_code_naive)
    # # get the kernel function from the compiled module
    runSudokuKernelFn_naive = mod_naive.get_function("runSudokuKernel")
    generateBoardFn_naive = mod_naive.get_function("generateBoard")

    #cuda run
    start = time.time()
    generateBoardFn(d_grid,d_moregrid,d_boardcount,block = (1, 1,1), )
    runSudokuKernelFn(d_moregrid,d_flag,d_actualgrid,grid=(int(np.ceil(float(d_boardcount.get()[0])/32)),1),block = (32, 1,1), )
    cudatime=time.time()-start
    cudatimes.append(cudatime)

    #cuda naive run
    start = time.time()
    generateBoardFn_naive(d_grid_naive,d_moregrid_naive,d_boardcount_naive,d_empty_spaces_i_naive,d_empty_spaces_j_naive,block = (1, 1,1), )
    runSudokuKernelFn_naive(d_moregrid_naive,d_actualgrid_naive,d_empty_spaces_i_naive,d_empty_spaces_j_naive,grid=(int(np.ceil(float(d_boardcount_naive.get()[0])/32)),1),block = (32, 1,1), )
    cudanaivetime=time.time()-start
    cudanaivetimes.append(cudanaivetime)

    #python run
    start = time.time()
    runSudoku(grid,empty_spaces)
    pythontime=time.time()-start
    pythontimes.append(pythontime)
    # print(checkGrid(d_actualgrid.get().reshape(size,size),size))
    # print(checkGrid(grid,size))

    # print the results
    print '---Verification---'
    print 'Verify optimized Cuda:',np.array_equal(d_actualgrid.get(),grid.flatten())
    print 'Verify naive Cuda:',np.array_equal(d_actualgrid_naive.get(),grid.flatten())

    print '---Timing---'
    print 'Time taken for python run:', pythontime
    print 'Time taken for Cuda Naive run:', cudanaivetime
    print 'Time taken for Cuda run:', cudatime

    print '---Results---'
    print "Python:\n",grid
    print "Cuda Naive:\n",d_actualgrid_naive.get().reshape(size,size)
    print "Cuda:\n",d_actualgrid.get().reshape(size,size)


print '---Optimized Cuda Vs Python speedup---'
CustomPrintSpeedUp(pythontimes,cudatimes)
print '---Optimized Cuda Vs Naive cuda speedup---'
CustomPrintSpeedUp(cudanaivetimes,cudatimes)

# Plot result     
plt.clf()
plt.cla()
plt.gcf()

pythonline,=plt.plot(X_AXIS_RECORD, pythontimes,label='python run')
plt.setp(pythonline,color='b')
cudaline,=plt.plot(X_AXIS_RECORD, cudatimes,label='cuda optimized run')
plt.setp(cudaline,color='g')
cudanaiveline,=plt.plot(X_AXIS_RECORD, cudanaivetimes,label='cuda naive run')
plt.setp(cudanaiveline,color='r')
plt.legend(handles=[pythonline,cudaline,cudanaiveline],bbox_to_anchor=(1, 1),loc=1)
axes = plt.gca()
axes.set_ylim([0,2])
plt.xlabel('Sudoku Size')
plt.ylabel('Time')
plt.title('Time taken Vs Sudoku Size')
plt.savefig('Cuda(1).png')


# Plot result     
plt.clf()
plt.cla()
plt.gcf()

pythonline,=plt.plot(X_AXIS_RECORD, pythontimes,label='python run')
plt.setp(pythonline,color='b')
cudaline,=plt.plot(X_AXIS_RECORD, cudatimes,label='cuda optimized run')
plt.setp(cudaline,color='g')
cudanaiveline,=plt.plot(X_AXIS_RECORD, cudanaivetimes,label='cuda naive run')
plt.setp(cudanaiveline,color='r')
plt.legend(handles=[pythonline,cudaline,cudanaiveline],bbox_to_anchor=(1, 1),loc=1)

plt.xlabel('Sudoku Size')
plt.ylabel('Time')
plt.title('Time taken Vs Sudoku Size')
plt.savefig('Cuda(2).png')

####################################### Main Code: Apply Steganography ###########################################

pythontimes=[]
cudatimes=[]
X_AXIS_RECORD=[]

gridString="0000NHRID0000R0S0U000GDSNHORUH0SIG000D0N0G0S000SRH0UOIDGUHON000I0O0G0000HSNDI0000"
# N-1,H-2,R-3,I-4,D-5, U-6,S-7,G-8,O-9
gridString=encodeMsg(gridString)
f = open('outputSudoku.txt','rb')
MAXBOARD = 3000
MAXDEPTH = 4

size = int(np.sqrt(len(gridString)))
grid = buildGrid(gridString,size)
# print(grid,size)
print(grid)
empty_spaces=createEmptySpacesNode(grid,size,False)
empty_spaces.sort(key=getPossibleValues)

# Define the input and output to process
# grid = buildGrid(gridString,size)
h_empty_spaces_i,h_empty_spaces_j= createEmptySpacesPositionList(grid,size,True)

X_AXIS_RECORD.append(len(h_empty_spaces_i))

h_boardcount=np.zeros((1,1)).astype(np.int32)
h_flag=np.zeros((1,1)).astype(np.int32)
h_grid = np.array(grid).flatten().astype(np.int32)
h_possibleValuesIdx=np.zeros((size,size)).astype(np.int32)
h_moregrid = np.zeros((MAXBOARD,size,size)).astype(np.int32)
h_actualgrid=np.zeros_like(h_grid).astype(np.int32)

d_boardcount = gpuarray.to_gpu(h_boardcount)
d_flag = gpuarray.to_gpu(h_flag)
d_grid = gpuarray.to_gpu(h_grid)
d_possibleValuesIdx=gpuarray.to_gpu(h_possibleValuesIdx)
d_moregrid = gpuarray.to_gpu(h_moregrid)
d_actualgrid=gpuarray.to_gpu(h_actualgrid)

# get the kernel code from the template 
# by specifying the constant MATRIX_SIZE
empty_spaces_length=len(h_empty_spaces_i);
kernel_code = kernel_code_template % {
    'size': size,
    'maxboard':MAXBOARD,
    'maxdepth':MAXDEPTH,
    'empty_spaces_length':empty_spaces_length
    }
    
# compile the kernel code
mod = compiler.SourceModule(kernel_code)
d_empty_spaces_i = mod.get_global('empty_spaces_i')[0]
d_empty_spaces_j = mod.get_global('empty_spaces_j')[0]
driver.memcpy_htod(d_empty_spaces_i,  np.array(h_empty_spaces_i).astype(np.int32))
driver.memcpy_htod(d_empty_spaces_j,  np.array(h_empty_spaces_j).astype(np.int32))

h_hiddenMsg=np.chararray((1,size))
d_hiddenMsg=gpuarray.to_gpu(h_hiddenMsg)
# # get the kernel function from the compiled module
runSudokuKernelFn = mod.get_function("runSudokuKernel")
generateBoardFn = mod.get_function("generateBoard")
decodeMsgFn = mod.get_function("decodeMsg")

#cuda run
start = time.time()
generateBoardFn(d_grid,d_moregrid,d_boardcount,block = (1, 1,1), )
runSudokuKernelFn(d_moregrid,d_flag,d_actualgrid,grid=(int(np.ceil(float(d_boardcount.get()[0])/32)),1),block = (32, 1,1), )
decodeMsgFn(d_actualgrid,d_hiddenMsg,block=(int(size),1,1))
cudatime=time.time()-start
cudatimes.append(cudatime)
# np.set_printoptions(threshold=np.inf)

#python run
start = time.time()
runSudoku(grid,empty_spaces)
py_hiddenMsg=decodeMsg(grid,size)
pythontime=time.time()-start
pythontimes.append(pythontime)
# print(checkGrid(d_actualgrid.get().reshape(size,size),size))
# print(checkGrid(grid,size))

# print the results
print '---Verification---'
print 'Verify Cuda:',np.array_equal(d_actualgrid.get(),grid.flatten())

print '---Timing---'
print 'Time taken for python run:', pythontime
print 'Time taken for Cuda run:', cudatime

print '---Results---'
print "Python:\n",grid
print "Cuda:\n",d_actualgrid.get().reshape(size,size)
print "Python Hidden Msg:", py_hiddenMsg
print "Cuda Hidden Msg:", d_hiddenMsg.get()
